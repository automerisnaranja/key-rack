<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <!-- Meson Build -->
  <id>@app_id@</id>
  <name>Key Rack</name>
  <translation type="gettext">key-rack</translation>
  <launchable type="desktop-id">@app_id@.desktop</launchable>

  <!-- General -->
  <summary>View and edit passwords</summary>
  <description>
    <p>
      Key Rack allows you to view, create and edit secrets, such as passwords or tokens, stored by apps. It supports both Flatpak secrets and system-wide keyrings.
    </p>
  </description>
  <content_rating type="oars-1.1" />

  <!-- Branding -->
  <developer_name>Felix Häcker, Sophie Herold</developer_name>
  <developer id="app.drey.KeyRack">
    <name>Felix Häcker, Sophie Herold</name>
  </developer>
  <branding>
    <color type="primary" scheme_preference="light">#f8e45c</color>
    <color type="primary" scheme_preference="dark">#e5a50a</color>
  </branding>

  <!-- License -->
  <project_license>GPL-3.0</project_license>
  <metadata_license>CC-BY-SA-4.0</metadata_license>

  <!-- URLs -->
  <url type="homepage">https://gitlab.gnome.org/sophie-h/key-rack</url>
  <url type="bugtracker">https://gitlab.gnome.org/sophie-h/key-rack/-/issues</url>
  <url type="vcs-browser">https://gitlab.gnome.org/sophie-h/key-rack/</url>
  <!-- TODO <url type="translate">https://l10n.gnome.org/module/key-rack/</url> -->
  <!-- TODO <url type="contribute">https://welcome.gnome.org/app/KeyRack/</url> -->

  <!-- Supported Hardware -->
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>

  <!-- Screenshots -->
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/-/project/23102/uploads/f37e08127a1336d74e255a7b959b0185/image.png</image>
      <caption>Overview of keyrings and Flatpak apps</caption>
    </screenshot>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/-/project/23102/uploads/4bd105b8bb37498ac0d7d55283448528/image.png</image>
      <caption>Details for a secret</caption>
    </screenshot>
  </screenshots>

  <!-- Releases -->
  <releases>
    <release version="0.4.0" date="2024-07-01">
      <description>
        This version bring a completely overhauled user interface. But the changes go much further than a better look. Key Rack now allows to view all host keyrings as well to change passwords of keyrings. Passwords can now be found more easily via the new search function and new passwords can be added via the interface.
      </description>
    </release>
    <release version="0.3.0" date="2024-01-06">
      <description>
        Add support for system wide secrets.
      </description>
    </release>
    <release version="0.2.0" date="2022-09-27">
      <description>
        <ul>
          <li>Fix a bunch of interface shortcomings</li>
          <li>Reload key list after changing an item</li>
          <li>Support for opening keyfiles with invalid entries</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" date="2022-09-27">
      <description>
        <p>The initial release brings the following features:</p>
        <ul>
          <li>View secrets and properties stored by apps</li>
          <li>Change stored secrets and their descriptions</li>
        </ul>
      </description>
    </release>
  </releases>
</component>
